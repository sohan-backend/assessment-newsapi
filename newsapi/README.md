# NEWSAPI-Blog Site 
<h4>
From Top navbar clicking fetch data options data will fetch form api and save in database.
In the settings file there is an api key which one can change by their account api key as developer accounts does not permit more than 100 request per day.
</h4>
<h2>
Features:
</h2>
<ul>
<li><b>Phase-1 Done </b>
<br>
<u>127.0.0.1:8000/phase1/</u><br>
Consuming data from APIs from NewsAPI.org and fetching top headlines which one also can filter by country and source.</li>
<li>
<b>Phase2- Done </b>
<br>
<u>127.0.0.1:8000/</u><br>
Secure NewsFeed For Used based on their Settings Functions (Exp: Multiple Sources, Filter By Country, Filter By Keyword)
<ol>
<li>Register</li>
<li>Login</li>
<li>Logout</li>
<li>Reset Password</li>
</ol>
</li>
<li>
<b>Phase 3- Done </b>
<br>
<ol>
<li>
I have set up a schedular that will update the newsfeed
periodically. The schedular will run periodically every 15 minutes using celery and redis server.
</li>
<li>Populating the news feed for users
based on the settings parameter.</li>
<li>Pagination</li>
</ol>

<br>

</li>
<li><b>Phase-4 - Done</b>
<ol>
    <li>if any of these keywords appear in the news of user
personalized news feed, the user will get an email notification (By Sendgrid) (</li>
</ol>
</li>
<li><b>Phase-5
<br>
Developing Api:
<br>
url : <u>http://127.0.0.1:8000/api/</u>
<ol>
<li>
Authorized api with jwt token key
</li>
<li>Personalized newsfeed api based on user settings</li>
<li>Access token : /api/token/</li>
</ol></b></li>

Used Technolgies :
</h2>
<ul>
<li>Python</li>
<li>Django v2.2.1</li>
<li>Celery</li>
<li>Redis Server</li>
<li>SQLite3</li>
<li>HTML</li>
<li>CSS</li>
<li>Bootstrap</li>
<li>Crispy Form</li>
<li>Font-Awesome</li>
<li>JavaScript</li>
</ul>

<h2>Installation</h2>
<h4>Create WebBlog virtual environment & goto the directory.
</h4>

<h4>
Linux -->
</h4>
<div>
virtualenv -p /usr/bin/python3 WebBlog
cd WebBlog/
</div>



<h4>
Windows -->
</h4>
<div>
virtualenv WebBlog
cd .\WebBlog\
</div>


<h2>Activate virtualenv :</h2>
<h4>Linux -->
</h4>
<div>
source bin/activate
</div>


<h4>Windows--></h4>
<div>
.\Scripts\activate
</div>

<h2>
clone the project in the WebBlog directory.
</h2>

<div>
https://gitlab.com/sohan-codesmith/newsapi.git
</div>

<h2>Install requirements.txt
</h2>
<div>
python -m pip install -r requirement.txt
</div>
<h2>
Now goto src/ directory and create db models.
</h2>


<div>
cd src/
python manage.py migrate
</div>

<h2>Run Server :</h2>
<div>
python manage.py runserver
</div>
Now go to<a href="http://127.0.0.1:8000/"><ul><li> http://127.0.0.1:8000/</li></ul> </a>

<h2>ScreenShots</h2>

<ol>

 <li>Home Page : </li><br>
<img src="https://gitlab.com/sohan-codesmith/newsapi/-/raw/main/screenshot/index.png"><br><br>

 <li>Phase-1 : </li><br>
<img src="https://gitlab.com/sohan-codesmith/newsapi/-/raw/main/screenshot/phase1.png"><br><br>
</ol>
</ul>


