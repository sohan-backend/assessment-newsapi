import json

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
import requests
from .models import Newsapi, TopHeadLines
from newsapi import NewsApiClient
from django.conf import settings
from users.models import Profile
from django.core.paginator import Paginator
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import viewsets
from .serializers import TopheadSerializer
from rest_framework.authentication import SessionAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from django.views import generic
from datetime import datetime
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from functools import reduce


def send_email_to_user(request):
    html = ''
    articles = TopHeadLines.objects.filter(new_entry=True)
    for l in articles:
        key = request.user.profile.preferred_keyward
        if key:
            key_list = key.split(',')
            print(key_list, type(key_list))
            if len(key_list) > 0:
                for key in key_list:
                    if key in l.title or key in l.description:
                        html += '<h3>' + l.title + '</h3>' + '<p>' + l.description + '</p>'
            l.new_entry = False
            l.save()
    print(html)
    to_list = []
    to_list.append(request.user.email)
    print(to_list)
    if not html == '':
        msg = EmailMessage("Automated Mail From News Api", html, settings.EMAIL_HOST_USER, to_list)
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        # send_mail('Automated Mail From News Api', html, settings.EMAIL_HOST_USER, to_list)
        print('Email sent ')


@login_required
def HomeView(request):
    # sending mail to new headline form celery
    send_email_to_user(request)

    key = request.user.profile.preferred_keyward
    news_source_ = request.user.profile.preferred_source

    jsonDec = json.decoder.JSONDecoder()
    news_source = []
    if news_source_:
        news_source = jsonDec.decode(news_source_)

    source = ''
    for l in news_source:
        source = source + l + ','

    if key:
        key_list = key.split(',')
        articles = TopHeadLines.objects.filter(
            reduce(lambda x, y: x | y, [Q(title__contains=word) for word in key_list]))
    elif news_source:
        articles = TopHeadLines.objects.filter(source__in=news_source)

    else:
        articles = TopHeadLines.objects.all().order_by('-publishedAt')

    p = Paginator(articles, 5)
    page_number = request.GET.get('page')
    try:
        page_obj = p.get_page(page_number)  # returns the desired page object
    except PageNotAnInteger:
        # if page_number is not an integer then assign the first page
        page_obj = p.page(1)
    except EmptyPage:
        # if page is empty then return last page
        page_obj = p.page(p.num_pages)

    newsapi = Newsapi.objects.all()
    unique_country = Newsapi.objects.values('country').distinct()

    context = {
        'articles': articles,
        'newsapi': newsapi,
        'unique_country': unique_country,
        'page_obj': page_obj
    }

    return render(request, 'blog/index.html', context)


def fetchApiData(request):
    url = 'https://newsapi.org/v2/top-headlines/sources?apiKey={}'.format(settings.API_KEY)
    from newsapi import NewsApiClient
    newsapi = NewsApiClient(api_key=settings.API_KEY)

    url_res = requests.get(url)
    json_obj = json.dumps(url_res.json())
    loaded_json = json.loads(json_obj)
    sources = loaded_json['sources']
    # print(sources)
    for l in sources:
        news = Newsapi.objects.filter(news_id=l['id'])
        if not news.exists():
            news_data = Newsapi(
                news_id=l['id'],
                news_name=l['name'],
                category=l['category'],
                language=l['language'],
                country=l['country']
            )
            news_data.save()

    top_headlines = newsapi.get_top_headlines()
    # top_headlines = newsapi.get_top_headlines(country='us')
    json_obj = json.dumps(top_headlines)
    loaded_json = json.loads(json_obj)
    articles = loaded_json['articles']
    print(articles)

    for l in articles:
        top_head = TopHeadLines.objects.filter(title=l['title'])
        if not top_head.exists():
            headline = TopHeadLines(
                source=l['source']['name'],
                author=l['author'],
                title=l['title'],
                description=l['description'],
                url=l['url'],
                urlToImage=l['urlToImage'],
                publishedAt=l['publishedAt'],
                content=l['content']
            )
            headline.save()

    return redirect('apiview')


def apiView(request):
    from newsapi import NewsApiClient
    newsapi = NewsApiClient(api_key=settings.API_KEY)

    if request.method == 'POST':
        news_source = request.POST.getlist('news_source')
        country = request.POST.get('country')
        source_str_ = ''
        for l in news_source:
            source_str_ = source_str_ + l + ','

        # country_str_ = ''
        # for l in country:
        #     country_str_ = country_str_ + l

        if news_source:
            url = settings.NEWS_API_BASE_URL + "/top-headlines?sources=" + source_str_ + "&apiKey=" + settings.API_KEY
        else:
            url = settings.NEWS_API_BASE_URL + "/top-headlines?country=" + country + "&apiKey=" + settings.API_KEY

        response = requests.get(url)
        articles = response.json()['articles']

        newsapi = Newsapi.objects.all()
        unique_country = Newsapi.objects.values('country').distinct()

        context = {
            'articles': articles,
            'newsapi': newsapi,
            'unique_country': unique_country
        }

        return render(request, 'blog/fetch.html', context)

    # /v2/top-headlines
    url = settings.NEWS_API_BASE_URL + "/top-headlines?country=us&apiKey=" + settings.API_KEY

    response = requests.get(url)
    articles = response.json()['articles']

    for article in articles:
        x = article['publishedAt']
        article['publishedAt'] = datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ').strftime("%B %d %Y, %I:%M %p")

    newsapi = Newsapi.objects.all()
    unique_country = Newsapi.objects.values('country').distinct()

    context = {
        'articles': articles,
        'newsapi': newsapi,
        'unique_country': unique_country
    }

    return render(request, 'blog/fetch.html', context)


from django.db.models import Q


class NewsViewApi(viewsets.ModelViewSet):
    # queryset = TopHeadLines.objects.all()
    serializer_class = TopheadSerializer

    authentication_classes = [SessionAuthentication, JWTAuthentication]
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        if not self.request.user.is_anonymous:
            prof_obj = Profile.objects.get(user=self.request.user)

        jsonDec = json.decoder.JSONDecoder()
        if prof_obj.preferred_source:
            news_source = jsonDec.decode(prof_obj.preferred_source)

        query = TopHeadLines.objects.filter(source__in=news_source).order_by('-id')

        return query
