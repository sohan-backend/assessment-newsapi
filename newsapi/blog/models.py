from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from users.models import Profile


# Create your models here.

class Newsapi(models.Model):
    news_id = models.CharField(max_length=255, blank=True, null=True)
    news_name = models.CharField(max_length=255, blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    language = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.news_id


class TopHeadLines(models.Model):
    news_source = models.ForeignKey(Newsapi, on_delete=models.DO_NOTHING, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    author = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    urlToImage = models.CharField(max_length=255, blank=True, null=True)
    publishedAt = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=255, blank=True, null=True)
    new_entry = models.BooleanField(default=True)

    def __str__(self):
        return (self.title + ' - '+ str(self.id))
