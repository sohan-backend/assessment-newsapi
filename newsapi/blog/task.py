from celery import shared_task
from time import sleep
from django.conf import settings
import requests
import json
from blog.models import Newsapi, TopHeadLines
from users.models import User
from django.core.mail import send_mail
from django.template.loader import render_to_string


# fetch and save top headlines in background

@shared_task
def fetch_data():
    url = 'https://newsapi.org/v2/top-headlines/sources?apiKey={}'.format(settings.API_KEY)
    from newsapi import NewsApiClient
    newsapi = NewsApiClient(api_key=settings.API_KEY)
    url_res = requests.get(url)
    json_obj = json.dumps(url_res.json())
    loaded_json = json.loads(json_obj)
    sources = loaded_json['sources']
    for l in sources:
        news = Newsapi.objects.filter(news_id=l['id'])
        if not news.exists():
            news_data = Newsapi(
                news_id=l['id'],
                news_name=l['name'],
                category=l['category'],
                language=l['language'],
                country=l['country']
            )
            news_data.save()

    top_headlines = newsapi.get_top_headlines()
    # top_headlines = newsapi.get_top_headlines(country='us')
    json_obj = json.dumps(top_headlines)
    loaded_json = json.loads(json_obj)
    articles = loaded_json['articles']

    for l in articles:
        top_head = TopHeadLines.objects.filter(title=l['title'])
        if not top_head.exists():
            headline = TopHeadLines(
                source=l['source']['id'],
                author=l['author'],
                title=l['title'],
                description=l['description'],
                url=l['url'],
                urlToImage=l['urlToImage'],
                publishedAt=l['publishedAt'],
                content=l['content']

            )
            headline.save()
