from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab
from datetime import timedelta

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'WebBlog.settings')
app = Celery('WebBlog')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.conf.beat_schedule = {
    'fetch_data_from_api': {
        'task': 'blog.task.fetch_data',
        'schedule': crontab(minute='*/15'),
        # 'args': ('admin@email.com',),
    }
}


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
