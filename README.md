#Assessment NEWSAPI- Site 
<h4>
I have dockerized the project which run simultanously django app, redis server for broker, celery for asynchronous task and celery-beat for periodic task which fetch data from newsapi every 15 minutes and save data in database if any new headline comes. 
From Top navbar clicking fetch data options data will fetch form api and save in database. I have also configured sengrid for mail service.In the settings file there is an api key which one can change by their account api key as developer accounts does not permit more than 100 request per day and sendgrid api key for mailing purpose.
</h4>
<h2>
Features:
</h2>
<ul>
<li><b>Phase-1 Done </b>
<br>
<u>127.0.0.1/phase1/</u><br>
Consuming data from APIs from NewsAPI.org and fetching top headlines which one also can filter by country and source.</li>
<li>
<b>Phase2- Done </b>
<br>
<u>127.0.0.1/</u><br>
Secure NewsFeed For Used based on their Settings Functions (Exp: Multiple Sources, Filter By Country, Filter By Keyword)
<ol>
<li>Register</li>
<li>Login</li>
<li>Logout</li>
<li>Reset Password</li>
</ol>
</li>
<li>
<b>Phase 3- Done </b>
<br>
<ol>
<li>
I have set up a schedular that will update the newsfeed
periodically. The schedular will run periodically every 15 minutes using celery and redis server.
</li>
<li>Populating the news feed for users
based on the settings parameter.</li>
<li>Pagination</li>
</ol>

<br>

</li>
<li><b>Phase-4 - Done</b>
<ol>
    <li>if any of these keywords appear in the news of user
personalized news feed, the user will get an email notification (By Sendgrid) (</li>
</ol>
</li>
<li><b>Phase-5
<br>
Developing Api:
<br>
url : <u>http://127.0.0.1/api/</u>
<ol>
<li>
Authorized api with jwt token key
</li>
<li>Personalized newsfeed api based on user settings</li>
<li>Access token : /api/token/</li>
</ol></b></li>

Used Technolgies :
</h2>
<ul>
<li>Python</li>
<li>Django v2.2.1</li>
<li>Docker</li>
<li>Celery</li>
<li>Redis Server</li>
<li>SQLite3</li>
<li>HTML</li>
<li>CSS</li>
<li>Bootstrap</li>
<li>Crispy Form</li>
<li>Font-Awesome</li>
<li>JavaScript</li>
</ul>

<h2>Build and Run Code:</h2>
<p>
docker-compose up --build -d
</p>
Now go to <a href="http://127.0.0.1">http://127.0.0.1</a> 

</ul>


